﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="OoO.Test" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
  <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <asp:HtmlEditorExtender runat="server" TargetControlID="TheBox" EnableSanitization="false" />

    <div>

      <asp:GridView runat="server" ID="TheGrid" OnRowDeleting="TheGrid_RowDeleting" DataKeyNames="Id">
        <Columns>
          <asp:CommandField ShowDeleteButton="true"/>
        </Columns>
      </asp:GridView>

      <p>
        <strong>Naposledy kliknuto na: </strong>
        <asp:Label runat="server" ID="KliknutoNaLabel" Text="nic" />
      </p>

      <p>
        <asp:TextBox runat="server" ID="TheBox" Width="500" Height="500" Text="<b>řešení</b>" />
      </p>

    </div>
  </form>
</body>
</html>
