﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OoO
{
  public partial class Test : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        this.BindData();
    }

    private void BindData()
    {
      var rows = new List<Row>()
      {
        new Row(1, "Nemam"),
        new Row(2, "rad"),
        new Row(3, "HtmlEditor"),
      };
      this.TheGrid.DataSource = rows;
      this.TheGrid.DataBind();
    }

    protected void TheGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
      var id = (int)e.Keys[0] ;
      this.KliknutoNaLabel.Text = id.ToString();
    }
  }


  class Row
  {
    public Row(int id, string text)
    {
      Id = id;
      Text = text ?? throw new ArgumentNullException(nameof(text));
    }

    public int Id { get; }

    public string Text { get; }

  }
}